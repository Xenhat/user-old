#!/bin/bash
# TODO: Abort if any of the commands fail by storing success status
#set -x
source /var/tmp/cpu
# Clean ramdisk if nothing is emerging
ps aux | grep -w emerge | grep python || rm -rvf /var/tmp/portage/*
function do_update() {
extra_opts="--autounmask --autounmask-write --autounmask-continue --oneshot"
IFS=' ' read -r -a array <<<"$@"
case "$1" in
--no-sync)
	echo "Warning: will only performs world rebuild"
	no_sync=1
	;;
--gcc)
	echo "Warning: as instructed, gcc upgrades will NOT be skipped. Exercise caution when using this flag. Consult the manual. Get a comfort blanket and some coffee."
	do_gcc=1
	;;
--pretend)
	pretend=1
	;;
--background)
	background=1
	;;
--fetchonly)
	fetchonly=1
	extra_opts="$extra_opts --fetchonly"
	;;
-*)
	echo "please write a short help text"
	exit 1
	;;
esac
if [ -z "${do_gcc}" ]; then
	extra_opts="$extra_opts --exclude=sys-devel/gcc"
fi
	if [ -z "$CPU_COUNT" ]; then
		CPU_COUNT="$(grep 'processor' /proc/cpuinfo -c)"
	fi
	if [ -z "$LOAD_AVERAGE" ]; then
		LOAD_AVERAGE="$(echo $CPU_COUNT \* 0.90 | bc)"
	fi
	if [ -n "$background" ]; then
		LOAD_AVERAGE="$(echo $CPU_COUNT \* 0.50 | bc)"
	fi
	echo "Portage will try to maintain a load average of $LOAD_AVERAGE"
	if [ -z "$no_sync" ]; then
		echo "============================="
		echo "=== Syncing ================="
		echo "============================="
		emerge-webrsync --quiet
		#emerge --sync
	fi
	# yes, no count to jobs, for "max possible
	#minim="--ignore-default-opts ${extra_opts} --jobs --load-average=${LOAD_AVERAGE} --quiet-build=y --buildpkg --ask=n --oneshot"
	#common="${minim} --newuse --update --deep"
	extra_opts="$extra_opts --update --newuse --changed-deps=y --deep --with-bdeps=y"
	echo "============================="
	echo "=== Updating Portage ========"
	echo "============================="
	portageopts="--ignore-default-opt --update --oneshot --quiet-build --jobs=$CPU_COUNT --load-average=$LOAD_AVERAGE"
	echo "emerge $portageopts portage"
	emerge $portageopts portage || exit 1
	echo "============================="
	echo "=== Updating @system ========"
	echo "============================="
	echo "emerge $common @system"
	emerge $extra_opts @system || exit 1
	#emerge $common @preserved-rebuild
	echo "============================="
	echo "=== Updating @world ========="
	echo "============================="
	echo "emerge $common @world"
	emerge $extra_opts @world || exit 1
	if [ -z "fetchonly" ]
	then
	echo "============================="
	echo "=== Depclean 1/2 ============"
	echo "============================="
	emerge --depclean --deep || exit 1
	echo "============================="
	echo "=== Running revdep-rebuild =="
	echo "============================="
	revdep-rebuild || exit 1
	echo "============================="
	echo "=== Depclean 2/2 ============"
	echo "============================="
	emerge --depclean --deep || exit 1
	#echo "============================="
	#echo "=== Cleaning up ============="
	#echo "============================="
	#eclean-dist --deep || exit 1
	#eclean-pkg --deep || exit 1
	fi
	return $?
}

# darkelf-features-0.1::darkelf
#
# This enables cleaning the distfiles after every emerge,
# to enable this feature set
# DARKELF_FEATURES="postmerge_distclean"
# in /etc/portage/make.conf or per command basis e.g.:
# DARKELF_FEATURES="postmerge_distclean" emerge ...

darkelf_postmerge_distclean() {
	echo cleaning distfiles for ${CATEGORY}/${PF}...
	if [ -z $DISTDIR ]; then
		echo ERROR: DISTDIR is empty!
		exit 1
	fi
	for f in $DISTDIR/*; do
		echo deleting "$(readlink -f $f)"...
		rm -r "$(readlink -f $f)"
	done
}

post_pkg_postinst() {
	if grep -q "postmerge_distclean" <<<$DARKELF_FEATURES; then
		darkelf_postmerge_distclean
	fi
}
source /etc/profile
env-update
do_update --fetchonly && echo "Validating that updates are done..." && do_update
unset common
# Discard all configuration updates.
etc-update --automode -7
