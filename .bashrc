#!/usr/bin/env bash
# Trap Ctrl+D and prevent immediate exit
#export IGNOREEOF=1
[[ $- != *i* ]] && return
# This still breaks :(
#if command -v tmux &> /dev/null && [ -n "$PS1" ] && [[ ! "$TERM" =~ "screen" ]] && [[ ! "$TERM" =~ "tmux" ]] && [ -z "$TMUX" ]
#then
#    if tmux list-session &>/dev/null
#    then
#        tmux attach \; new-window
#    else
#        tmux new -s auto
#    fi
#fi

if [ -n "$XENRC_DIR" ]
then
    source "$XENRC_DIR/bash.rc"
fi
